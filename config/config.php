<?php

	session_start();
	date_default_timezone_set('Asia/Manila');

	$base_url = 'http://localhost/leave-ms';
	$_SESSION['base_url'] = $base_url;

	$logged_in = isset($_SESSION['user_id']);

	include 'database.php';

	if ($logged_in) {

		/* Get the logged user info */
		$logged_user_id = $_SESSION['user_id'];
		$logged_user = $dbcon->prepare("SELECT * FROM users WHERE id = :id");
		$logged_user->bindparam(':id', $logged_user_id);
		$logged_user->execute();

		$logged_user_data = $logged_user->fetch(PDO::FETCH_ASSOC);

	}

	function redirect($link) {
		$base_url = 'http://localhost/leave-ms';
		header("Location: $base_url/$link");
	}

?>
