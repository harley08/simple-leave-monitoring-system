<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $page_title; ?></title>
	<link rel="stylesheet" href="<?php echo $base_url ?>/assets/vendor/bootstrap/css/bootstrap.min.css" type="text/css">
  	<link rel="stylesheet" href="<?php echo $base_url ?>/assets/vendor/datatables/dataTables.bootstrap4.css" type="text/css">
	<link href="<?php echo $base_url ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/styles.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/sb-admin.css" type="text/css">
    <script src="<?php echo $base_url ?>/assets/js/jquery.min.js"></script>
</head>
<body>