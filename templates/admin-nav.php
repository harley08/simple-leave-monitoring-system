<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
	<a class="navbar-brand" href="<?php echo $base_url ?>"><b><?php echo date('F j, Y'); ?></b></a>
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
    	<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
    		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
    			<a class="nav-link" href="index.php">
    				<i class="fa fa-fw fa-dashboard"></i>
    				<span class="nav-link-text">Dashboard</span>
    			</a>
    		</li>
    		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Employees">
    			<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseEmployees" data-parent="#exampleAccordion">
    				<i class="fa fa-fw fa-user"></i>
    				<span class="nav-link-text">Employees</span>
    			</a>
    			<ul class="sidenav-second-level collapse" id="collapseEmployees">
    				<li>
    					<a href="employees.php">Show Employees</a>
    				</li>
                    <li>
                        <a href="add-leave.php">Add Leave</a>
                    </li>
    			</ul>
    		</li>
    		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reports">
    			<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseReports" data-parent="#exampleAccordion">
    				<i class="fa fa-fw fa-book"></i>
    				<span class="nav-link-text">Reports</span>
    			</a>
    			<ul class="sidenav-second-level collapse" id="collapseReports">
    				<li><a href="<?php echo $base_url ?>/application/dashboard/reports.php"><b><?php echo date('F'); ?></b> Reports</a></li>
                        <li><a href="<?php echo $base_url ?>/application/dashboard/history.php">History</a></li>
    			</ul>
    		</li>
				<li class="nav-item" title="Account Settings" data-toggle="tooltip" data-placement="right"><a href="account-settings.php" class="nav-link"><i class="fa fa-fw fa-user"></i> <span class="nav-link-text">Account Settings</span></a></li>
    	</ul>
    	<ul class="navbar-nav sidenav-toggler">
    		<li class="nav-item">
    			<a class="nav-link text-center" id="sidenavToggler">
    				<i class="fa fa-fw fa-angle-left"></i>
    			</a>
    		</li>
    	</ul>
    	<ul class="navbar-nav ml-auto">
    		<li class="nav-item">
    			<!--<form class="form-inline my-2 my-lg-0 mr-lg-2">
    				<div class="input-group">
    					<input class="form-control" type="text" placeholder="Search Employee...">
    					<span class="input-group-append">
    						<button class="btn btn-primary" type="button">
    							<i class="fa fa-search"></i>
    						</button>
    					</span>
    				</div>
    			</form>-->
    		</li>
    		<li class="nav-item">
    			<a class="nav-link" data-toggle="modal" data-target="#exampleModal">
    				<i class="fa fa-fw fa-sign-out"></i>Logout</a>
    			</a>
    		</li>
    	</ul>
    </div>
</nav>
