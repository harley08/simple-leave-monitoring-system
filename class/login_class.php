<?php
	
	class dataLogin {

		private $db;

		function __construct($dbcon)
		{
			$this->db = $dbcon;
		}

		public function login($username, $password) {
			
			try {

				$login = $this->db->prepare("SELECT * FROM users WHERE username = :username AND password = :password");
				$login->bindparam(":username", $username);
				$login->bindparam(":password", $password);
				$login->execute();

				$login_data = $login->fetch(PDO::FETCH_ASSOC);

				if ($login->rowCount() == 1) {
					$_SESSION['user_id'] = $login_data['id'];
					return true;
				} else {
					return false;
				}

			} catch(PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

	}

?>