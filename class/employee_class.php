<?php
	class dataEmployee {

		private $db;

		function __construct($dbcon) {
			$this->db = $dbcon;
		}

		public function AddEmployee($fname, $mname, $lname, $office, $position, $rate_per_day) {

			try {

				$stmt = $this->db->prepare("INSERT INTO employees (firstname, middlename, lastname, office, position, rate_per_day) VALUES (:firstname, :middlename, :lastname, :office, :position, :rate_per_day)");
				$stmt->bindparam(":firstname", $fname);
				$stmt->bindparam(":middlename", $mname);
				$stmt->bindparam(":lastname", $lname);
				$stmt->bindparam(":office", $office);
				$stmt->bindparam(":position", $position);
				$stmt->bindparam(":rate_per_day", $rate_per_day);
				$stmt->execute();

				return true;

			} catch(PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function showEmployees() {

			try {

				$stmt = $this->db->prepare("SELECT * FROM employees");
				$stmt->execute();

				if ($stmt->rowCount() != null)
				{
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						?>
						<tr>
							<td><?php echo $row['firstname'] . ' ' . $row['lastname'] ?></td>
							<td><?php echo $row['office'] ?></td>
							<td><?php echo $row['position'] ?></td>
							<td><?php echo $row['rate_per_day'] ?></td>
							<td class="text-center">
								<a href="edit-employee.php?id=<?php echo $row['id'] ?>">
									<i class="fa fa-fw fa-edit"></i>
								</a>
								<a href="actions.php?remove_employee&id=<?php echo $row['id'] ?>">
									<i class="fa fa-fw fa-remove"></i>
								</a>
							</td>
						</tr>
						<?php
					}
				}
				else
				{
					?>
					<tr>
						<td class="text-center" colspan="4">no data</td>
					</tr>
					<?php
				}

			} catch (PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function updateEmployee($fname, $mname, $lname, $office, $position, $rate_per_day, $id) {

			try {

				$stmt = $this->db->prepare("UPDATE employees SET "
					."firstname = :fname, "
					."middlename = :mname, "
					."lastname = :lname, "
					."office = :office, "
					."position = :position, "
					."rate_per_day = :rate_per_day WHERE "
					."id = :id");

				$stmt->bindparam(":fname", $fname);
				$stmt->bindparam(":mname", $mname);
				$stmt->bindparam(":lname", $lname);
				$stmt->bindparam(":office", $office);
				$stmt->bindparam(":position", $position);
				$stmt->bindparam(':rate_per_day', $rate_per_day);
				$stmt->bindparam(':id', $id);
				$stmt->execute();

				return true;

			} catch(PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function employeeOnDropdown() {

			try {

				$stmt = $this->db->prepare("SELECT * FROM employees");
				$stmt->execute();

				if ($stmt->rowCount() != null) {

					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['firstname'] . ' ' . $row['lastname'] ?></option>
						<?php
					}

				} else {
					?>
					<option value="">no data yet</option>
					<?php
				}

			} catch (PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function addLeave($employee_id, $start_date, $end_date, $type_of_leave, $leave_spent) {

			try {

				$stmt = $this->db->prepare("INSERT INTO leaves (employee_id,  start_date, end_date, type_of_leave, leave_spent, month, year) VALUES (:employee_id,  :start_date, :end_date, :type_of_leave, :leave_spent, :month, :year)");
				$stmt->bindparam(':employee_id', $employee_id);
				$stmt->bindparam(":start_date", $start_date);
				$stmt->bindparam(":end_date", $end_date);
				$stmt->bindparam(":type_of_leave", $type_of_leave);
				$stmt->bindparam(":leave_spent", $leave_spent);
				$stmt->bindparam(":month", date('F'));
				$stmt->bindparam(":year", date('Y'));

				$stmt->execute();

				return true;

			} catch (PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function showLeaves() {

			try {

				$stmt = $this->db->prepare("SELECT * FROM leaves ORDER BY id DESC");
				$stmt->execute();

				if ($stmt->rowCount() != null) {

					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

						$id = $row['employee_id'];

						$data = $this->db->prepare("SELECT * FROM employees WHERE id = :id");
						$data->bindparam(':id', $id);
						$data->execute();
						$employee = $data->fetch(PDO::FETCH_ASSOC);

						?>
						<div class="list-group-item list-group-item-action">
							<div class="media">
								<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="" />
								<div class="media-body">
									<strong><?php echo $employee['firstname'] . ' ' . $employee['lastname'] ?></strong> had <?php echo $row['type_of_leave'] ?> leave at <?php echo $row['leave_spent'] ?>
									<div class="text-muted smaller">From <?php echo date('F j, Y', strtotime($row['start_date'])) ?> to <?php echo date('F j, Y', strtotime($row['end_date'])) ?></div>
								</div>
							</div>
						</div>
						<?php
					}

				} else {
					?>
					<div class="text-center" style="margin: 15px;">
						<b>no data yet</b>
					</div>
					<?php
				}

			} catch(PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function removeEmployee($id) {

			try {

				$stmt = $this->db->prepare("DELETE FROM employees WHERE id = :id");
				$stmt->bindparam(':id', $id);
				$stmt->execute();

				return true;

			} catch (PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function showReportsPerMonth($month, $year) {

			try {

				$stmt = $this->db->prepare("SELECT * FROM employees");
				$stmt->execute();

				if ($stmt->rowCount() != null) {

					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

						/* Get the number of leave */
						$get_day = $this->db->prepare("SELECT * FROM leaves WHERE month = :month AND year = :year AND employee_id = :id");
						$get_day->bindparam(":month", $month);
						$get_day->bindparam(":year", $year);
						$get_day->bindparam(":id", $row['id']);
						$get_day->execute();

						$no_of_leave = 0;

						while ($date = $get_day->fetch(PDO::FETCH_ASSOC)) {

							$start_date = date_create($date['start_date']);
							$end_date = date_create($date['end_date']);

							$datediff = date_diff($start_date, $end_date);

							$no_of_leave += $datediff->format('%a%');

						}

						/* Get the number of days per month */
						$days_per_month = 0;

						switch($month) {
							case 'January':
								$days_per_month = 31;
								break;
							case 'February':
								$days_per_month = 28;
								break;
							case 'March':
								$days_per_month = 31;
								break;
							case 'April':
								$days_per_month = 30;
								break;
							case 'May':
								$days_per_month = 31;
								break;
							case 'June':
								$days_per_month = 30;
								break;
							case 'July':
								$days_per_month = 31;
								break;
							case 'August':
								$days_per_month = 31;
								break;
							case 'September':
								$days_per_month = 30;
								break;
							case 'October':
								$days_per_month = 31;
								break;
							case 'November':
								$days_per_month = 30;
								break;
							case 'December':
								$days_per_month = 31;
								break;
							default:
								break;
						}

						$salary_per_month = $row['rate_per_day'] * $days_per_month;

						$less_payout = $no_of_leave * $row['rate_per_day'];

						$total_payout = $salary_per_month - $less_payout;

						?>
						<tr>
							<td><?php echo $row['firstname'] . ' ' . $row['lastname'] ?></td>
							<td class="text-center"><?php echo $row['position'] ?></td>
							<td class="text-center"><?php echo $no_of_leave; ?></td>
							<td class="text-center"><?php echo $row['rate_per_day'] ?></td>
							<td class="text-center"><b>P<?php echo $total_payout; ?></b></td>
							<td class="text-center">
								<a href="print.php?leave_id=<?php echo $row['id'] ?>" target="_blank">
									<i class="fa fa-fw fa-print"></i>
								</a>
							</td>
						</tr>
						<?php
					}

				} else {
					?>
					<tr>
						<td class="text-center" colspan="4">no employees</td>
					</tr>
					<?php
				}

			} catch(PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}

		}

		public function updateAccount($username, $new_password) {

			try {

				$stmt = $this->db->prepare("UPDATE users SET username = :username, password = :password");
				$stmt->bindparam(":username", $username);
				$stmt->bindparam(":password", md5($new_password));
				$stmt->execute();

				return true;

			} catch (PDOException $ex) {
				echo $ex->getMessage();
				return false;
			}


		}
	}
?>
