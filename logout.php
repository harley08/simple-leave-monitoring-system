<?php
	
	include 'config/config.php';

	if (session_destroy()) {
		redirect('');
	}

?>