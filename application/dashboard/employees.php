<?php

	$page_title = "Employees";
		
	include '../../config/config.php';

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
				<li class="breadcrumb-item active">Employees</li>
			</ol>

			<?php
				if (isset($_GET['success'])) {
					?>
					<div class="alert alert-success">
						<i class="fa fa-fw fa-check"></i> New employee successfully added!
					</div>
					<?php
				}
				if (isset($_GET['error'])) {
					?>
					<div class="alert alert-danger">
						<i class="fa fa-fw fa-remove"></i> Failed to add new employee!
					</div>
					<?php
				}
			?>

			<?php
				if (isset($_GET['deleted'])) {
					?>
					<div class="alert alert-success">
						<i class="fa fa-fw fa-check"></i> Employee successfully removed!
					</div>
					<?php
				}
				if (isset($_GET['error_delete'])) {
					?>
					<div class="alert alert-danger">
						<i class="fa fa-fw fa-remove"></i> Failed to remove employee!
					</div>
					<?php
				}
			?>

		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#newEmployeeModal"><i class="fa fa-fw fa-plus"></i> New Employee</button>
		<div style="margin: 15px"></div>
		<table id="dataTable" class="table table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Office</th>
					<th>Position</th>
					<th>Rate Per Day</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>
					<?php
					include '../../class/employee_class.php';
					$dataEmployee = new dataEmployee($dbcon);
					$dataEmployee->showEmployees();
				?>
				</tbody>
		</table>
	</div>
</div>

<?php include '../../modals/employees.php'; ?>

<?php
	
	include '../../templates/footer.php';

?>