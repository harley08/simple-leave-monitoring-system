<?php

	$page_title = "Edit Employee";
		
	include '../../config/config.php';

	if (!$logged_in) {
		redirect('application/login');
	}

	if (isset($_GET['id'])){

		$id = $_GET['id'];

		$get_data = $dbcon->prepare("SELECT * FROM employees WHERE id = :id");
		$get_data->bindparam(":id", $id);
		$get_data->execute();

		$data = $get_data->fetch(PDO::FETCH_ASSOC);

	} else {
		redirect('application/dashboard/employees.php');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
				<li class="breadcrumb-item "><a href="employees.php">Employees</a></li>
				<li class="breadcrumb-item active">Update Employee</li>
			</ol>

			<?php
				if (isset($_GET['success'])) {
					?>
					<div class="alert alert-success">
						<i class="fa fa-fw fa-check"></i> Employee data successfully updated!
					</div>
					<?php
				}
				if (isset($_GET['error'])) {
					?>
					<div class="alert alert-danger">
						<i class="fa fa-fw fa-remove"></i> Failed to update employee data!
					</div>
					<?php
				}
			?>
			<div class="card">
				<div class="card-header">
					<span class="card-title"><b><i class="fa fa-fw fa-edit"></i> Update Employee</b></span>
				</div>
				<div class="card-body">
					<form method="post" action="actions.php">
						<div class="form-group">
							<label><b>First Name:</b></label>
							<div class="col-md-4">
								<input type="text" name="firstname" class="form-control" value="<?php echo $data['firstname'] ?>" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Middle Name:</b></label>
							<div class="col-md-4">
								<input type="text" name="middlename" class="form-control" value="<?php echo $data['middlename'] ?>" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Last Name:</b></label>
							<div class="col-md-4">
								<input type="text" name="lastname" class="form-control" value="<?php echo $data['lastname'] ?>" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Office:</b></label>
							<div class="col-md-4">
								<input type="text" name="office" class="form-control" value="<?php echo $data['office'] ?>" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Position:</b></label>
							<div class="col-md-4">
								<input type="text" name="position" class="form-control" value="<?php echo $data['position'] ?>" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Rate Per Day:</b></label>
							<div class="col-md-4">
								<input type="number" name="rate_per_day" class="form-control" value="<?php echo $data['rate_per_day'] ?>" />
							</div>
						</div>
						<input type="hidden" name="action" value="updateEmployee" />
						<input type="hidden" name="id" value="<?php echo $id ?>" />
						<button type="submit" class="btn btn-primary">Update</button>
						<a href="employees.php" class="btn btn-default">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<?php include '../../modals/employees.php'; ?>

<?php
	
	include '../../templates/footer.php';

?>