<?php

	$page_title = "Add Leave";
		
	include '../../config/config.php';

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
				<li class="breadcrumb-item "><a href="employees.php">Employees</a></li>
				<li class="breadcrumb-item active">New Leave</li>
			</ol>

			<?php
				if (isset($_GET['success'])) {
					?>
					<div class="alert alert-success">
						<i class="fa fa-fw fa-check"></i> Employee leave successfully added! Add new one?
					</div>
					<?php
				}
				if (isset($_GET['error'])) {
					?>
					<div class="alert alert-danger">
						<i class="fa fa-fw fa-remove"></i> Failed to add employee leave!
					</div>
					<?php
				}
			?>
			<div class="card">
				<div class="card-header">
					<span class="card-title"><b><i class="fa fa-fw fa-plus"></i> Add Leave</b></span>
				</div>
				<div class="card-body">
					<form method="post" action="actions.php">
						<div class="form-group">
							<label><b>Employee Name:</b></label>
							<div class="col-md-4">
								<select name="employee_id" class="form-control" required>
									<option value="">-- select employee --</option>

									<?php
										include '../../class/employee_class.php';
										$dataEmployee = new dataEmployee($dbcon);

										$dataEmployee->employeeOnDropdown();
									?>

								</select>
							</div>
						</div>
						<div class="form-group">
							<label><b>Start Date:</b></label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="start_date" />
							</div>
						</div>
						<div class="form-group">
							<label><b>End Date:</b></label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="end_date" />
							</div>
						</div>
						<div class="form-group">
							<label><b>Type of Leave:</b></label>
							<div class="col-md-4">
								<select name="type_of_leave" class="form-control" required>
									<option value="">-- choose --</option>
									<option value="Vacation">Vacation</option>
									<option value="To seek employment">To seek employment</option>
									<option value="Sick">Sick</option>
									<option value="Maternity">Maternity</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label><b>Where leave spent:</b></label>
							<div class="col-md-4">
								<textarea name="leave_spent" class="form-control" required></textarea>
							</div>
						</div>
						<input type="hidden" name="action" value="addLeave" />
						<button type="submit" class="btn btn-primary">Add</button>
						<a href="employees.php" class="btn btn-default">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<?php include '../../modals/employees.php'; ?>

<?php
	
	include '../../templates/footer.php';

?>