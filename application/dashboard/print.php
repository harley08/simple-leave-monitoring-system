<?php
	$page_title = "Print Report";

	include '../../config/config.php';

	if (!isset($_GET['leave_id']) || !$logged_in) {
		redirect('');
	}

	$leave_id = $_GET['leave_id'];

	$leave = $dbcon->prepare("SELECT * FROM leaves WHERE id = :id");
	$leave->bindparam(':id', $leave_id);
	$leave->execute();

	$leave_data = $leave->fetch(PDO::FETCH_ASSOC);

	/* Get the employee information */
	$employee = $dbcon->prepare("SELECT * FROM employees WHERE id = :id");
	$employee->bindparam(':id', $leave_data['employee_id']);
	$employee->execute();
	$employee_data = $employee->fetch(PDO::FETCH_ASSOC);

	$middlename = $employee_data['middlename'];

	/* Get the number of leave */
	$get_day = $dbcon->prepare("SELECT * FROM leaves WHERE id = :id");
	$get_day->bindparam('id', $leave_id);
	$get_day->execute();

	$no_of_leave = 0;

	while ($date = $get_day->fetch(PDO::FETCH_ASSOC)) {
	
		$start_date = date_create($date['start_date']);
		$end_date = date_create($date['end_date']);

		$datediff = date_diff($start_date, $end_date);

		$no_of_leave += $datediff->format('%a%');

	}

	/* Get the number of days per month */
	$days_per_month = 0;
	switch($leave_data['month']) {
		case 'January':
			$days_per_month = 31;
			break;
		case 'February':
			$days_per_month = 28;
			break;
		case 'March':
			$days_per_month = 31;
			break;
		case 'April':
			$days_per_month = 30;
			break;
		case 'May':
			$days_per_month = 31;
			break;
		case 'June':
			$days_per_month = 30;
			break;
		case 'July':
			$days_per_month = 31;
			break;
		case 'August':
			$days_per_month = 31;
			break;
		case 'September':
			$days_per_month = 30;
			break;
		case 'October':
			$days_per_month = 31;
			break;
		case 'November':
			$days_per_month = 30;
			break;
		case 'December':
			$days_per_month = 31;
			break;
		default:
			break;
	}

	$salary_per_month = $employee_data['rate_per_day'] * $days_per_month;
	$days_with_pay = $days_per_month - $no_of_leave;

	include '../../templates/header.php';
?>

<script type="text/javascript">
	print();
</script>

<style type="text/css">
	p {
		margin:  1px;
	}
	.municipal_logo {
		margin-top: 20px;
		margin-bottom: -120px;
		padding-left: 20px;
	}
	.header {
		margin-top: 40px;
	}
	.print {
		background-color: #ffffff;
  		width: 8.5in;
  		height: 13in;
  		padding: 10px;
  		border: 1px solid #DDDDDD;
  		margin: auto;
	}
	.line {
		padding-bottom: 10px;
		border-bottom: 1px solid #000000;
		margin-bottom: 10px;
	}
</style>

<div id="page-top" class="print shadow">
	<div class="municipal_logo">
		<img src="<?php echo $base_url ?>/assets/images/bansud-logo.png" width="70px" />
	</div>
	<div class="text-center header">
		<p>Republic of the Philippines</p>
		<p>Province of Oriental Mindoro</p>
		<p><b>MUNICIPALITY OF BANSUD</b></p>
		<p>Bansud, Oriental Mindoro</p>
	</div>
	<div style="margin: 15px;"></div>
	<div class="text-center">
		<h4><b>APPLICATION FOR LEAVE</b></h4>
	</div>
	<div style="margin: 15px;"></div>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colspan="1">
				1. Office/Agency<br>
				<b><?php echo $employee_data['office'] ?></b>
				</td>
				<td colspan="2">
					2. Name
					<span style="margin-left: 50px;">(Last)</span>
					<span style="margin-left: 60px;">(First Name)</span>
					<span style="margin-left: 70px;">(Middle Initial)</span><br>
					<span style="margin-left: 100px;"><b><?php echo $employee_data['lastname'] ?></b></span>
					<span style="margin-left: 90px;"><b><?php echo $employee_data['firstname'] ?></b></span>
					<span style="margin-left: 100px;"><b><?php echo $middlename[0] ?>.</b></span>
				</td>
			</tr>
			<tr>
				<td colspan="1">
					3. Date of Filing<br>
					<b><?php echo date('F j, Y', strtotime($leave_data['date_filed'])) ?></b>
				</td>
				<td colspan="1">
					4. Position<br>
					<b><?php echo $employee_data['position'] ?></b>
				</td>
				<td colspan="1">
					5. Monthly Salary<br>
					<b>P<?php echo $salary_per_month ?></b>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="text-center">
		<p><b>DETAILS OF APPLICATION</b></p>
	</div>
	<table width="100%" style="margin: 30px; margin-top: 10px">
		<tr>
			<td valign="top">6. a) Type of Leave<br>
				<?php
					if ($leave_data['type_of_leave'] == 'Vacation') {
						?>
						<div style="margin-bottom: -20px;"><i class="fa fa-check"></i></div>
						<?php
					}
				?>
				___________________ Vacation<br>
				<?php
					if ($leave_data['type_of_leave'] == 'To seek employment') {
						?>
						<div style="margin-bottom: -20px;"><i class="fa fa-check"></i></div>
						<?php
					}
				?>
				___________________ To seek employment<br>
				<?php
					if ($leave_data['type_of_leave'] == 'Sick') {
						?>
						<div style="margin-bottom: -20px;"><i class="fa fa-check"></i></div>
						<?php
					}
				?>
				___________________ Sick<br>
				<?php
					if ($leave_data['type_of_leave'] == 'Maternity') {
						?>
						<div style="margin-bottom: -20px;"><i class="fa fa-check"></i></div>
						<?php
					}
				?>
				___________________ Maternity<br>
				___________________ Other (Specify)
			</td>
			<td valign="top">
				6. b) Where leave will spent<br>
				1. In case of vacation leave<br>
				__________________________ within the Philippines<br>
				__________________________ Abroad
				<div style="margin: 20px"></div>
				2. In case of sick leave<br>
				__________________________ In hospital<br>
				__________________________ At home
			</td>
		</tr>
		<tr>
			<td valign="top">
				<div style="margin-top: 20px;"></div>
				6. c) Number of Working Days Applied for:<br>
				<div style="margin-bottom: -20px;"><b><?php echo $no_of_leave ?></b></div>
				_____ Days<br>
				<div style="margin-bottom: -20px; margin-left: 100px"><b><?php echo date('F j, Y', strtotime($leave_data['start_date'])) ?></b></div>
				From period ____________________<br>
				<div style="margin-bottom: -20px; margin-left: 100px"><b><?php echo date('F j, Y', strtotime($leave_data['end_date'])) ?></b></div>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Up to ____________________
			</td>
			<td valign="top">
				<div style="margin-top: 20px;">
					6. d) Commutation<br>
					Requested __________________ Not Required
				</div>
			</td>
		</tr>
	</table>
	<div class="line"></div>
	<div class="text-center">
		<b>DETAILS OF ACTION ON APPLICATION</b>
	</div>
	<div style="margin: 20px;"></div>
	<table width="100%">
		<tr>
			<td width="70%">
				<b>PILAR R. VITTO</b><br>
				MGDH-1
			</td>
			<td width="30%">
				<u><b>ANGEL M. SAULONG</b></u><br>
				Authorized Officials
			</td>
		</tr>
	</table>
	<div class="line"></div>
	<table width="100%">
		<tr>
			<td width="50%">
				.7 c) APPROVED FOR<br>
				<div style="margin-bottom: -20px; margin-left: 75px"><b><?php echo $no_of_leave ?></b></div>
				________________________ Days without Pay<br>
				<div style="margin-bottom: -20px; margin-left: 75px"><b><?php echo $days_with_pay ?></b></div>
				________________________ Days with Pay<br>
				________________________ Other Specify
			</td>
			<td width="50%">
				.7 d) DISAPPROVED TO <br>
				_________________________________<br>
				_________________________________
			</td>
		</tr>
	</table>
	<div class="text-center">
		<div style="margin: 50px"></div>
		______________________________________<br>
		<small>(Signature)</small><br>
		<b>ANGEL M. SAULONG</b><br>
		Municipal Mayor
	</div>
</div>

<?php 

	include '../../templates/footer.php';
?>