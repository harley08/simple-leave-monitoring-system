<?php
	if (isset($_POST['action']) && !empty($_POST['action'])) {

		$action = $_POST['action'];

		switch($action) {
			case 'AddEmployee':
				AddEmployee();
				break;
			case 'showEmployees':
				showEmployees();
				break;
			case 'updateEmployee':
				updateEmployee();
				break;
			case 'addLeave':
				addLeave();
				break;
			case 'updateAccount':
				updateAccount();
				break;
			default:
			break;
		}

	}

	if (isset($_GET['remove_employee']) && isset($_GET['id'])) {
		$id = $_GET['id'];
		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		if ($dataEmployee->removeEmployee($id)) {
			redirect('application/dashboard/employees.php?deleted');
		} else {
			redirect('application/dashboard/employees.php?error_delete');
		}
	}

	function AddEmployee()
	{
		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		$fname = $_POST['firstname'];
		$mname = $_POST['middlename'];
		$lname = $_POST['lastname'];
		$office = $_POST['office'];
		$position = $_POST['position'];
		$rate_per_day = $_POST['rate_per_day'];

		if ($dataEmployee->AddEmployee($fname, $mname, $lname, $office, $position, $rate_per_day)) {
			redirect('application/dashboard/employees.php?success');
		} else {
			redirect('application/dashboard/employees.php?error');
		}
	}

	function showEmployees()
	{
		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		$dataEmployee->showEmployees();
	}

	function updateEmployee()
	{
		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		$fname = $_POST['firstname'];
		$mname = $_POST['middlename'];
		$lname = $_POST['lastname'];
		$office = $_POST['office'];
		$position = $_POST['position'];
		$rate_per_day = $_POST['rate_per_day'];
		$id = $_POST['id'];

		if ($dataEmployee->updateEmployee($fname, $mname, $lname, $office, $position, $rate_per_day, $id)) {
			redirect('application/dashboard/edit-employee.php?id='.$id.'&success');
		} else {
			redirect('application/dashboard/edit-employee.php?id='.$id.'&error');
		}


	}

	function addLeave()
	{
		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		$employee_id = $_POST['employee_id'];
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$type_of_leave = $_POST['type_of_leave'];
		$leave_spent = $_POST['leave_spent'];

		if ($dataEmployee->addLeave($employee_id, $start_date, $end_date, $type_of_leave, $leave_spent)) {
			redirect('application/dashboard/add-leave.php?success');
		} else {
			redirect('application/dashboard/add-leave.php?error');
		}


	}

	function updateAccount()
	{

		include '../../config/config.php';
		include '../../class/employee_class.php';
		$dataEmployee = new dataEmployee($dbcon);

		$username = $_POST['username'];
		$current_password = $_POST['current_password'];
		$new_password = $_POST['new_password'];
		$re_new_password = $_POST['re_new_password'];

		if ($new_password == $re_new_password) {
			if ($dataEmployee->updateAccount($username, $new_password)) {
				redirect('application/dashboard/account-settings.php?updated');
			} else {
				redirect('application/dashboard/account-settings.php?error');
			}
		}
		else {
			redirect('application/dashboard/account-settings.php?password_mismatch');
		}

	}
?>
