<?php

	$page_title = "Reports";
		
	include '../../config/config.php';

	/* Get the total employess */
	$employees = $dbcon->prepare("SELECT * FROM employees");
	$employees->execute();

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
				<li class="breadcrumb-item active"><b><?php echo date('F'); ?></b> Reports</li>
			</ol>
			<table id="dataTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Employee</th>
						<th class="text-center">Position</th>
						<th class="text-center">No. of Leave</th>
						<th class="text-center">Rate Per Day</th>
						<th class="text-center">Total Payout</th>
						<th class="text-center">Print</th>
					</tr>
				</thead>
				<tbody>
					
					<?php
						include '../../class/employee_class.php';
						$dataEmployee = new dataEmployee($dbcon);
						$dataEmployee->showReportsPerMonth(date('F'), date('Y'));
					?>
					</tbody>
			</table>
		</div>
	</div>
</div>

<?php
	
	include '../../templates/footer.php';

?>
