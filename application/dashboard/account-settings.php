<?php

	$page_title = "Account Settings";

	include '../../config/config.php';

	/* Get the total employess */
	$employees = $dbcon->prepare("SELECT * FROM employees");
	$employees->execute();

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
				<li class="breadcrumb-item active">Account Settings</li>
			</ol>

      <?php

      if (isset($_GET['updated'])) {
        ?>
        <div class="alert alert-success">Account settings successfully updated!</div>
        <?php
      }

      if (isset($_GET['error'])) {
        ?>
        <div class="alert alert-danger">Failed to update account settings!</div>
        <?php
      }

      if (isset($_GET['password_mismatch'])) {
        ?>
        <div class="alert alert-danger">Re-typed password does not match to your new password!</div>
        <?php
      }

      ?>

      <div class="card">
        <div class="card-header">
          Update Account Settings
        </div>
        <div class="card-body">
          <form method="post" action="actions.php">
            <div class="form-group">
              <label>Username:</label>
              <div class="col-md-4">
                <input type="text" name="username" class="form-control" /></div>
            </div>
            <div class="form-group">
              <label>Current Password:</label>
              <div class="col-md-4">
                <input type="password" name="current_password" class="form-control" /></div>
            </div>
            <div class="form-group">
              <label>New Password:</label>
              <div class="col-md-4">
                <input type="password" name="new_password" class="form-control" /></div>
            </div>
            <div class="form-group">
              <label>Re-typ New Password:</label>
              <div class="col-md-4">
                <input type="password" name="re_new_password" class="form-control" /></div>
            </div>
            <input type="hidden" name="action" value="updateAccount" />
            <button type="submit" class="btn btn-primary">Change Settings</button>
            <a href="index.php" class="btn btn-default">Cancel</a>
          </form>
        </div>
      </div>
		</div>
	</div>
</div>

<?php

	include '../../templates/footer.php';

?>
