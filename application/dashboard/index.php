<?php

	$page_title = "Dashboard";
		
	include '../../config/config.php';

	/* Get the total employess */
	$employees = $dbcon->prepare("SELECT * FROM employees");
	$employees->execute();

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
				<li class="breadcrumb-item active">My Dashboard</li>
			</ol>
			<div class="row">
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-primary o-hidden h-100">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-user"></i>
							</div>
							<div class="mr-5"><?php echo $employees->rowCount() ?> Total Employees!</div>
							</div>
							<a class="card-footer text-white clearfix small z-1" href="employees.php">
								<span class="float-left">View Details</span>
								<span class="float-right">
									<i class="fa fa-angle-right"></i>
								</span>
							</a>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
						<i class="fa fa-bell-o"></i> Leave Feeds
					</div>
					<div class="list-group list-group-flush small">

						<?php
							include '../../class/employee_class.php';
							$dataEmployee = new dataEmployee($dbcon);
							$dataEmployee->showLeaves();
						?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	
	include '../../templates/footer.php';

?>
