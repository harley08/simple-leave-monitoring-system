<?php

	$page_title = "History";
		
	include '../../config/config.php';

	/* Get the total employess */
	$employees = $dbcon->prepare("SELECT * FROM employees");
	$employees->execute();

	if (!$logged_in) {
		redirect('application/login');
	}

	include '../../templates/header.php';

?>

<div class="fixed-nav sticky-footer bg-dark" id="page-top">
	<?php include '../../templates/admin-nav.php'; ?>
	<div style="margin: 50px;"></div>
	<div class="content-wrapper">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
				<li class="breadcrumb-item active">History</li>
			</ol>
			<form method="post" action="">
				<div class="row">
					<div class="col-md-2">
						<select name="month" class="form-control" required>
							<option value="">select month</option>
							<option value="January">January</option>
							<option value="February">February</option>
							<option value="March">March</option>
							<option value="April">April</option>
							<option value="May">May</option>
							<option value="June">June</option>
							<option value="July">July</option>
							<option value="August">August</option>
							<option value="September">September</option>
							<option value="October">October</option>
							<option value="November">November</option>
							<option value="December">December</option>
						</select>
					</div>
					<div class="col-md-2">
						<select name="year" class="form-control" required>
							<?php
								$leave = $dbcon->prepare("SELECT * FROM leaves GROUP BY year");
								$leave->execute();

								if ($leave->rowCount() != null) {

									while ($row = $leave->fetch(PDO::FETCH_ASSOC)) {
										?>
										<option value="<?php echo $row['year'] ?>"><?php echo $row['year'] ?></option>
										<?php
									}

								} else {
									?>
									<option value="">no data</option>
									<?php
								}
							?>
						</select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i></button>
					</div>
				</div>
			</form>
			<div style="margin: 15px"></div>
			<table id="dataTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Employee</th>
						<th class="text-center">Position</th>
						<th class="text-center">No. of Leave</th>
						<th class="text-center">Rate Per Day</th>
						<th class="text-center">Total Payout</th>
						<th class="text-center">Print</th>
					</tr>
				</thead>
				<tbody>
					
					<?php

						if (isset($_POST['month'])) {
							$month = $_POST['month'];
							$year = $_POST['year'];

							include '../../class/employee_class.php';
							$dataEmployee = new dataEmployee($dbcon);
							$dataEmployee->showReportsPerMonth($month, $year);
						}

					?>

				</tbody>
			</table>
		</div>
	</div>
</div>

<?php
	
	include '../../templates/footer.php';

?>
