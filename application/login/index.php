<?php

	$page_title = "Login";
		
	include '../../config/config.php';

	if ($logged_in) {
		redirect('application/dashboard');
	}

	include '../../templates/header.php';

?>

<div class="container">
	<div class="form-signin">
		<div style="margin: 5em;"></div>
		<div class="card">
			<div class="card-body">
				<figure class="avatar text-center">
					<img src="<?php echo $base_url ?>/assets/images/no-avatar.jpg" width="100px" />
				</figure>

				<div class="alert alert-info">You must login first!</div>

				<?php

				if (isset($_GET['login_error'])) {
					?>
					<div class="alert alert-danger">
						Error occured. Please try again.
					</div>
					<?php
				}

				?>

				<form method="post" action="login.php">
					<div class="form-group">
						<label>Username:</label>
						<input type="text" name="username" class="form-control" placeholder="Username" required />
					</div>
					<div class="form-group">
						<label>Password:</label>
						<input type="password" name="password" class="form-control" placeholder="*************" required />
					</div>
					<input type="hidden" name="action" value="login" />
					<button type="submit" class="btn btn-primary btn-block">Login</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
	
	include '../../templates/footer.php';

?>