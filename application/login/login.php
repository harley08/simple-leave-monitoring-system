<?php
	if (isset($_POST['action']) && !empty($_POST['action'])) {

		$action = $_POST['action'];

		switch($action) {
			case 'login':
				login();
				break;
			default:
				break;
		}

	}

	function login() {
		include '../../config/config.php';
		include '../../class/login_class.php';

		$dataLogin = new dataLogin($dbcon);

		$username = $_POST['username'];
		$password = $_POST['password'];
		$password = md5($password);

		if ($dataLogin->login($username, $password)) {
			redirect('application/dashboard');
		} else {
			redirect('application/login?login_error');
		}
	}
?>