<div id="newEmployeeModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span class="modal-title"><i class="fa fa-fw fa-plus"></i> New Employee</span>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form id="addEmployeeForm" method="post" action="actions.php">
					<div class="form-group">
						<label for="firstname">Frst Name:</label>
						<input type="text" name="firstname" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="middlename">Middle Name:</label>
						<input type="text" name="middlename" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="lastname">Last Name:</label>
						<input type="text" name="lastname" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="office">Office:</label>
						<input type="text" name="office" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="position">Position:</label>
						<input type="text" name="position" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="monthly_salary">Rate Per Day:</label>
						<input type="number" name="rate_per_day" class="form-control" required placeholder="P00.00" />
					</div>
					<input type="hidden" name="action" value="AddEmployee" />
					<button type="submit" class="btn btn-primary">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>